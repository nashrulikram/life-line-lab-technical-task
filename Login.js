import React, { useState } from 'react';
// import axios from 'axios';
// import CryptoJS from 'crypto-js';
import './Login.css';

function Login({ onLogin }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    // try {
    //   const response = await axios.get('https://randomuser.me/api/?seed=lll');
    //   const user = response.data.results[0].login;

    //   const hashedPassword = CryptoJS.SHA256(password).toString();

    //   if (username === user.username && hashedPassword === user.sha256) {
    //     onLogin(username, password);
    //   } else {
    //     setError('Invalid username or password');
    //   }
    // } catch (err) {
    //   setError('An error occurred during login');
    // }

    // For now, let's directly call onLogin with dummy data
    onLogin('example_user', 'example_password');
  };

  return (
    <div className="login-container">
      <form className="login-form" onSubmit={handleSubmit}>
        <h2>Login</h2>
        {/* {error && <p className="error">{error}</p>} */}
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        <button type="submit">Login</button>
      </form>
    </div>
  );
}

export default Login;

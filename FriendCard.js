import React, { useState } from 'react';
import './FriendCard.css';
import Modal from './Modal';

function FriendCard({ friend }) {
  const [isOpen, setIsOpen] = useState(false);

  const handleOpenModal = () => {
    setIsOpen(true);
  };

  const handleCloseModal = () => {
    setIsOpen(false);
  };

  const { picture, name } = friend;

  return (
    <div className="friend-card" onClick={handleOpenModal}>
      <img src={picture.medium} alt={`${name.first} ${name.last}`} />
      <h3>{name.first} {name.last}</h3>
      <Modal isOpen={isOpen} onClose={handleCloseModal} friend={friend} />
    </div>
  );
}

export default FriendCard;

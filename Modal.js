import React from 'react';
import './Modal.css';

function Modal({ isOpen, onClose, friend }) {
  if (!isOpen) return null;

  const { picture, name, email, dob, location, phone } = friend;

  const handleModalClick = (e) => {
    e.stopPropagation(); 
  };

  return (
    <div className="modal-overlay" onClick={onClose}>
      <div className="modal-content" onClick={handleModalClick}>
        <button className="modal-close" onClick={onClose}>Close</button>
        <img src={picture.large} alt={`${name.first} ${name.last}`} />
        <div>
          <p><strong>Name:</strong> {name.first} {name.last}</p>
          <p><strong>Email:</strong> {email}</p>
          <p><strong>Date of Birth:</strong> {new Date(dob.date).toLocaleDateString()}</p>
          <p><strong>Address:</strong> {location.street.number} {location.street.name}, {location.city}, {location.state} {location.postcode}</p>
          <p><strong>Phone:</strong> {phone}</p>
        </div>
      </div>
    </div>
  );
}

export default Modal;

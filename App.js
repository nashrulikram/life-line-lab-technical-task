import React from 'react';
import './App.css';
// import Login from './Login';
import FriendList from './FriendList';

function App() {
  return (
    <div className="App">
      <h1>Friends List</h1>
      {/* <Login /> */}
      <FriendList />
    </div>
  );
}

export default App;

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import FriendCard from './FriendCard';
import './FriendList.css';

function FriendList() {
  const [friends, setFriends] = useState([]);
  const [filteredFriends, setFilteredFriends] = useState([]);
  const [page, setPage] = useState(1);
  const [inputPage, setInputPage] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [error, setError] = useState('');

  const fetchFriends = async (pageNumber) => {
    try {
      const response = await axios.get(`https://randomuser.me/api/?seed=lll&page=${pageNumber}&results=25`);
      setFriends(response.data.results);
      setError('');
    } catch (error) {
      console.error('Error fetching friends:', error);
      setError('An error occurred. Please try again later.');
    }
  };

  useEffect(() => {
    fetchFriends(page);
  }, [page]);

  useEffect(() => {
    setFilteredFriends(
      friends.filter((friend) =>
        friend.name.first.toLowerCase().includes(searchQuery.toLowerCase()) ||
        friend.name.last.toLowerCase().includes(searchQuery.toLowerCase())
      )
    );
  }, [friends, searchQuery]);

  const handleNextPage = () => {
    setPage(page + 1);
  };

  const handlePrevPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  const handleInputChange = (e) => {
    setInputPage(e.target.value);
    setError('');
  };

  const handleGoToPage = (e) => {
    e.preventDefault();
    const pageNumber = parseInt(inputPage);
    if (isNaN(pageNumber) || pageNumber < 1 || pageNumber > 20) {
      setError('Please enter a valid page number (1-20).');
    } else {
      setPage(pageNumber);
      setInputPage('');
    }
  };

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
  };

  return (
    <div>
      <div className="search-bar">
        <input
          type="text"
          value={searchQuery}
          onChange={handleSearch}
          placeholder="Search for a friend..."
        />
      </div>
      <div className="friend-list">
        {filteredFriends.map((friend, index) => (
          <FriendCard key={index} friend={friend} />
        ))}
      </div>
      <div className="pagination">
        <button onClick={handlePrevPage} disabled={page === 1}>Previous</button>
        <button onClick={handleNextPage}>Next</button>
        <form onSubmit={handleGoToPage}>
          <input
            type="text"
            value={inputPage}
            onChange={handleInputChange}
            placeholder="Page Number"
          />
          <button type="submit">Go</button>
        </form>
      </div>
      {error && <p className="error">{error}</p>}
    </div>
  );
}

export default FriendList;
